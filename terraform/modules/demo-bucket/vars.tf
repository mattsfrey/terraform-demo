variable "name" {
  type        = string
  description = "Name for GCS bucket"
}

variable "region" {
  type        = string
  description = "Region where GCS bucket is located"
}