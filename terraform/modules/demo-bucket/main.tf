resource "google_storage_bucket" "demo-bucket" {
  name          = var.name
  location      = var.region
  force_destroy = true
}