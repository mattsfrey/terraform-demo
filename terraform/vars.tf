variable "project_id" {
  type        = string
  description = "GCP Project ID"
  default     = "novolabs-devops-poc"    
}

variable "region" {
  type = string
  description = "gcp region"
  default = "us-central1"
}

variable "zone" {
  type        = string
  description = "gcp zone location"
  default     = "us-central1-a"
}