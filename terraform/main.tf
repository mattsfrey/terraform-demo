terraform {
  backend "http" {
  }
}

provider "google" {
  project = var.project_id
  region  = var.region
  zone    = var.zone
}

resource "google_storage_bucket" "main-demo-bucket" {
  name          = "novolabsdemo-bucket"
  location      = "US-CENTRAL1"
  force_destroy = true
}


# module "terraform-demo-bucket" {
#   source  = "./modules/demo-bucket"
#   name    = "novolabsdemo-buckettwo"
#   region  = "US-CENTRAL1"
# }